// importar el modulo para MongoDB=> mongoose
const mongoose = require('mongoose');

//Importar las variables de entorno
require('dotenv').config({path: 'variables.env'});

const conexionDB = async () => {
    try {
        
        await mongoose.connect(process.env.URL_MONGODB, {});
        console.log('Conexion establecida con MongoDB Atlas desde /config/db');

    } catch (error) {
        console.log('Eror en la conexion a la base de datos')
        console.log(error);
        process.exit(1);
    }
}
// exportar para utilizar en otros archivos
module.exports = conexionDB;