// forma convencional de ES6 => estandar convencional de JS
//import express from express

// forma sistema nativo de NodeJS
const express = require('express');

const router = express.Router();

// importar el modulo para MongoDB=> mongoose
//const mongoose = require('mongoose');

//Importar las variables de entorno
//require('dotenv').config({path: 'variables.env'});

// Importar la ruta a la conexion con la base de datos
const conectarDB = require('./config/db.js');

let app = express();

//app.use('/', function(req, res){
//    res.send('Hola Mundo')
// });

app.use(router);

//conexion a la base de datos con variables de entorno
//console.log(process.env.URL_MONGODB);
//    mongoose.connect(process.env.URL_MONGODB)
//    .then(function(){
//    console.log('Conexion establecida con MongoDB Atlas.')
//    })
//    .catch(function(){
//      console.log(e)
//    })

// y archivo externo a la ruta principal(index)
conectarDB();

// Esquema del modelo => la estructura
/* const usuarioSchema = mongoose.Schema({
    id: Number,
    nombre: String,
    apellido: String,
    edad: Number,
    telefono: Number,
    email: String

}); */

//const modelUsuario = mongoose.model('usuario',usuarioSchema);

// CRUD => create
/* modelUsuario.create(
    {
    id: 3,
    nombre: "Andrea",
    apellido: "Barrera",
    edad: 56,
    telefono: 214554,
    email: "ab@mail.com"
    },
    (error) => {
        //console.log('ingreso funcion flecha');
        if(error) return console.log(error);
        //console.log(error)
        //console.log('Sale de la funcion flecha despues del if (error)');
    }
     );
 */
 // CRUD => READ
/* modelUsuario.find((error, usuarios) => {

    if(error) return console.log(error);
    console.log(usuarios);
});
 */
// CRUD => UPDATE
/* modelUsuario.updateOne({id: 11}, {id: 2, nombre: "Catalina", apellido: "Zapata", edad: 45, telefono: 7894546, email: "cz@mail.com"}, (error) => {
if (error) return console.log(error);
});*/

// CRUD => DELETE
/* modelUsuario.deleteOne({edad: 31},(error) => {
    if (error) return console.log(error);

});  */

//------------------------------------------------------
// ##### DESCENTRALIZACIoN DEL CRUD #########
// ##### Rutas Respecto al CRUD     #########
//------------------------------------------------------

// uso de archivos tipo json en la app
app.use(express.json());
// CORS => Mecanismo o reglas de seguridad para el control de las peticiones http
const cors = require('cors');

// solicitudes al CRUD => el conroladro
const crudUsuarios = require ('./controller/controlUsuarios')
// Establecer las rutas respecto al CRUD
// CRUD => create
router.post('/', crudUsuarios.crear);
// CRUD => read
router.get('/', crudUsuarios.obtener);
// CRUD => update
router.put('/:id', crudUsuarios.actualizar);
// CRUD => detele
router.delete('/:id', crudUsuarios.eliminar);


//router.get('/metget', function(req, res){
//    res.send('Esta ruta es en respuesta al metodo GET')
//});

router.get('/metget', (req, res) => {
    res.send('Esta ruta es en respuesta al metodo GET')
});

router.post('/metget', function(req, res){
    res.send('Esta ruta es en respuesta al metodo POST de la ruta /metget')
});

router.post('/metpost', function(rep, res){
    res.send('Esta ruta es en respuesta al metodo POST de la ruta /metpost')

    //Conexion con la base de datos
//   console.log('Mensaje antes de la conexion');
//    const user = 'Jimena';
//    const psw = 'jimena';
//   const db= 'easyparking';
//    const url= `mongodb+srv://${user}:${psw}@proyectoeasyparking.ic9xj.mongodb.net/${db}?retryWrites=true&w=majority`;

//    mongoose.connect(url)
//    .then(function(){
//    console.log('Conexion establecida con MongoDB Atlas.')
//    })
//    .catch(function(){
//    console.log(e)
//    })
});


//Conexion con la base de datos
//const user = 'Jimena';
//const psw = 'jimena';
//const db= 'easyparking';
//const url= `mongodb+srv://${user}:${psw}@proyectoeasyparking.ic9xj.mongodb.net/${db}?retryWrites=true&w=majority`;

//mongoose.connect(url)
//.then(function(){
//    console.log('Conexion establecida con MongoDB Atlas.')
//})
//.catch(function(){
//    console.log(e)
//})

app.listen(4000);

console.log('La aplicacion se ejecuta desde el puerto 4000')