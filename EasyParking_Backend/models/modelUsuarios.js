// importar el modulo para MongoDB=> mongoose
const mongoose = require('mongoose');


const usuarioSchema = mongoose.Schema({
    id: Number,
    nombre: String,
    apellido: String,
    edad: Number,
    telefono: Number,
    email: String

},
{
    versionKey: false,
    timestamps: true,
}
);
// Para utilizar en archivos externos debo exportar como modulo
module.exports = mongoose.model('usuarios', usuarioSchema);