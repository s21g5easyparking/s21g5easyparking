// importar el modelo (esquema) de producto que definimos
const modelUsuario = require('../models/modelUsuarios');
// Exportar en diferente varibales los metodos para el CRUD
// create CRUD (crear)
exports.crear = async(req, res) =>{

    try {
        let usuario;

        usuario = new modelUsuario({
            id: 4,
            nombre: "Lorena",
            apellid: "Martinez",
            edad: 36,
            telefono: 1234566,
            email: "lm@mail.com"
        });

        await usuario.save()

        res.send(usuario);
        
    } catch (error) {
        console.log(error);
        res.status(500).send('Erro al guardar usuario');
    }
}
//read CRUD

exports.obtener = async (req, res) => {

    try {
        
        const usuario = await modelUsuario.find();
        res.json(usuario);

    } catch (error){
        res.status(500).send('Error al obtener el/los usuarios');
    }
}
// Update CRUD
exports.actualizar = async (req, res) => {
    try {
        
        const usuario = await modelUsuario.findById(req.params.id);

        if (!usuario)
        {
            console.log(usuario);
            res.status(404).json({msg: 'El usuario no existe'});
        }
        else{
            await modelUsuario.findByIdAndUpdate({_id: req.params.id}, {nombre: "pedro", apellido:"ramirez", telefono: 7847524, email: "pr@mail.com" })
        res.json({msg: 'Usuario actulizado correctamente'})
            }
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al actualizar el usuario');
        
    }
}

//Delete CRUD
exports.eliminar = async (req, res) => {
    try{
        const usuario = await modelUsuario.findById(req.params.id);
        
        if(!usuario){
            console.log(usuario);
            res.status(404).json({msg: 'El producto no existe'});
        }
        else{
            await modelUsuario.findByIdAndRemove(req.params.id);
            res.json({msg: 'Usuario eliminado correctamente'});
        }
    }catch(error){
        console.log(error);
        res.status(500).send('Error al eliminar el usuario');
    }
    
}